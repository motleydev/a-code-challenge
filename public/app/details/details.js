angular.module( 'ngHarpBoilerplate.details', [
  'ngHarpBoilerplate.user',
  'ui.router',
  'ui.bootstrap'
  ])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'details', {
    url: '/details/:gameid',
    views: {
      "main": {
        controller: 'DetailsController',
        templateUrl: 'app/details/details.html',
        resolve: {
          users: ['Users', function(Users){
            return Users;
          }]
        }
      }
    },
    data:{ pageTitle: 'What is It?' }
  });
})

.controller( 'DetailsController', ['$scope', '$state', '$stateParams','users', function($scope, $state, $stateParams,users) {

  $scope.users = [];

  var currentUser = Parse.User.current();
  var placeholderEmail = 'placeholdr@test.com';
  if((currentUser)){
    $scope.currentUser = currentUser;
    $scope.gravatar = users.getGravatar(currentUser.attributes.email || placeholderEmail);
  }else{
    $state.transitionTo('login');
  }

  console.log($stateParams.gameid);
  

  $scope.getGameDetails = function(){
    users.getGameDetails($stateParams.gameid).then(
      function(data){
        $scope.userData = data.attributes;
        console.table($scope.userData);
        //$scope.users = newData;
        $scope.$apply();
      },
      function(err){
        console.log(err);
      });
  };

  $scope.getGameDetails();

  $scope.logOut = function(){
    users.logOut().then(
      function(data){
        $state.transitionTo('login');
      },
      function(err){
        console.log(err);
      });
  };


}])
