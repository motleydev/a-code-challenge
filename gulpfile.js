var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var harp        = require('harp');

/**
 * Serve the Harp Site from the public directory
 */
 gulp.task('serve', function () {
  harp.server(__dirname + '/public', {
    port: 9000
  }, function () {
    browserSync({
      proxy: "localhost:9000",
      open: false,
      /* Hide the notification. It gets annoying */
      notify: {
        styles: ['opacity: 0', 'position: absolute']
      }
    });
    /**
     * Watch for scss changes, tell BrowserSync to refresh main.css
     */
     gulp.watch("public/**/*.less", function () {
      reload("main.css", {stream: true});
    });
    /**
     * Watch for all other changes, reload the whole page
     */
     gulp.watch([
      "public/**/*.ejs",
      "public/**/*.jade",
      "public/**/*.json",
      "public/**/*.js"
      ], function () {
        reload();
      });
   })
});

/**
 * Default task, running just `gulp` will compile the sass,
 * compile the harp site, launch BrowserSync & watch files.
 */
 gulp.task('default', ['serve']);