angular.module( 'ngHarpBoilerplate.match', [])
.factory('Match', function(){

	function Match(){

		this.points = {
			value: 0,
			add: function(points){
				this.value += points;
			}
		};
		this.completedWords = [];
	};

	return (Match);
});