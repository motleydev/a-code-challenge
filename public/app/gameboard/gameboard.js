/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
 angular.module( 'ngHarpBoilerplate.gameboard', [
  'ngHarpBoilerplate.user',
  'ngHarpBoilerplate.words',
  'ngHarpBoilerplate.match',
  'ui.router'
  ])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
 .config(function config( $stateProvider ) {
  $stateProvider.state( 'gameboard', {
    url: '/gameboard',
    views: {
      "main": {
        controller: 'GameboardController',
        controllerAs: 'board',
        templateUrl: 'app/gameboard/gameboard.html',
        resolve: {
          users: ['Users', function(Users){
            return Users;
          }],
          words: ['Words', function(Words){
            return Words
          }],
          match: ['Match', function(Match){
            return Match;
          }]
        }
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

/**
 * And of course we define a controller for our route.
 */
 .controller( 'GameboardController', ['$scope', '$state', '$timeout', 'words','users', 'match', function($scope, $state, $timeout, words, users, match) {

  var currentUser = Parse.User.current();
  var placeholderEmail = 'placeholdr@test.com';
  if((currentUser)){
    $scope.currentUser = currentUser;
    $scope.gravatar = users.getGravatar(currentUser.attributes.email || placeholderEmail);
    $scope.user = currentUser.attributes;
  }else{
    $state.transitionTo('login');
  }

  
  

  $scope.logOut = function(){
    users.logOut().then(
      function(data){
        $state.transitionTo('login');
      },
      function(err){
        console.log(err);
      });
  };

  // Load Services

  $scope.m = new match();

  $scope.w = words;
  $scope.words = [];
  $scope.currentIndex = words.currentIndex;
  $scope.jumbledWord = $scope.words[0];

  words.getAllWords().then(
    function(data){

      $scope.words = data;
      

    },
    function(result){
      console.log(result);

    });
  

  /**
  *
  * Timer
  *
  **/
  
  $scope.time = 40;

  var countDown = function() {

    if ($scope.time !== 0)
    {
      $scope.time -= 1;
      $timeout(countDown, 1000);  
    }
    else
    {
      $scope.$broadcast('game-over');
    }
    
  }


  /**
  *
  * Game Dynamics
  *
  **/
  
  $scope.gameStarted = false;
  $scope.gameIsOver = false;
  $scope.gameIsPlaying = function(){
    if($scope.gameStarted && !$scope.gameIsOver){
      return true;
    }else{
      return false;
    }
  };

  $scope.$on('game-over', function(){
    $scope.gameIsOver = true;
    
    users.addHighScore($scope.m).then(
      function(data){
        $state.transitionTo('scoreboard');
      },function(err){
        console.log(err);

      })
  });
  
  $scope.playGame = function(){
    startRound();
    $scope.gameStarted = true;
    $timeout(countDown, 1000);
  }


  $scope.guessed = function(){
    var input = $scope.guessBox;
    var inputArray = input.split('');
    
    // Check the status of Guesses
    if(inputArray.length <= $scope.guess.length)
    {

      if($scope.possiblePoints > 0){
        $scope.possiblePoints -= 1;
      }
      else{
        $scope.possiblePoints = 0;
      }

      $scope.guess = inputArray;

    }
    // What to do after theoretically enough letters were entered
    else if(inputArray.length === words.getWordAsArray().length)
    {
      //  If it is a winner.
      if (input.toLowerCase() === words.getWordAsText().toLowerCase())
      {
        //Add Available Points to score.
        $scope.m.points.add($scope.possiblePoints);

        // Add to list of accomplishment
        $scope.m.completedWords.push({
          word: words.getWordAsText(),
          percent: Math.floor(($scope.possiblePoints / words.getPointValue())*100)
        });

        newRound();
      }

    }
    // Not enough letters
    else
    {
      $scope.guess = inputArray;
    }
  };


  //Helpers

  function startRound(){
    // Fetch the jumbled version.
    $scope.jumbledWord = words.getJumbledWord();

    var helperText = '``````````````````````'+
    'THE WORD IS: '+words.getWordAsText()+
    '``````````````````````';
    console.log(helperText);

    setTimeout(function(){
      document.getElementById("guessBox").focus();
    }, 50);

    
    // Set the Possible Points.
    $scope.possiblePoints = words.getPointValue();
    // Reset the input field
    $scope.guessBox = '';
    $scope.guess = [];
  }

  function newRound(){
    // Iterate next word.
    words.getNextWord();
    startRound();

  }


}])


