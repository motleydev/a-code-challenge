Parse.initialize("ww1RP9pcRnGOIY4Z746gBMHbvdSCzRMV7AfqtVWT", "SPF4luP6V7hT00QrIONHcnOU8ZuCfaYcgXgAOKnz");


angular.module( 'ngHarpBoilerplate', [
  'ngHarpBoilerplate.signup',
  'ngHarpBoilerplate.login',
  'ngHarpBoilerplate.gameboard',
  'ngHarpBoilerplate.scoreboard',
  'ngHarpBoilerplate.details',
  'ui.router'
  ])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/gameboard' );
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | ngHarpBoilerplate' ;
    }
  });
});

