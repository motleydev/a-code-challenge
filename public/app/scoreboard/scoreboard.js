angular.module( 'ngHarpBoilerplate.scoreboard', [
  'ngHarpBoilerplate.user',
  'ui.router',
  'ui.bootstrap'
  ])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'scoreboard', {
    url: '/scoreboard',
    views: {
      "main": {
        controller: 'ScoreBoardController',
        templateUrl: 'app/scoreboard/scoreboard.html',
        resolve: {
          users: ['Users', function(Users){
            return Users;
          }]
        }
      }
    },
    data:{ pageTitle: 'What is It?' }
  });
})

.controller( 'ScoreBoardController', ['$scope', '$state' ,'users', function($scope, $state, users) {

  $scope.users = [];

  var currentUser = Parse.User.current();
  var placeholderEmail = 'placeholdr@test.com';
  if((currentUser)){
    $scope.currentUser = currentUser;
    $scope.gravatar = users.getGravatar(currentUser.attributes.email || placeholderEmail);
  }else{
    $state.transitionTo('login');
  }


  $scope.listUsers = function(){
    users.listUsers().then(
      function(data){
        var newData = data.map(function(user){
          console.log(user.id);
          return user;
        });
        $scope.users = newData;
        $scope.$apply()
      },
      function(err){
        console.log(err);
      })
  }

  $scope.listUsers();

  $scope.logOut = function(){
    users.logOut().then(
      function(data){
        $state.transitionTo('login');
      },
      function(err){
        console.log(err);
      });
  };


}])
