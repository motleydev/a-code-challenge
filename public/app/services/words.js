angular.module( 'ngHarpBoilerplate.words', [])
.factory('Words', function($http, $log, $q, $rootScope){
	
	var wsvc = {};

	wsvc.data = [];

	wsvc.getAllWords = function(){
		var deferred = $q.defer();
		var req = {
			method: 'GET',
			url: 'https://api.parse.com/1/classes/Food',
			headers: {
				'X-Parse-Application-Id': 'ww1RP9pcRnGOIY4Z746gBMHbvdSCzRMV7AfqtVWT',
				'X-Parse-REST-API-Key': 'joPaz4TSH65DylSQ9sJy1xerhDJITVwqJMgUxCsm' 
			}
		};

		$http(req)
		.success(function(data){
			wsvc.data = data.results;
			deferred.resolve(data.results);
		})
		.error(function(msg, code){
			deferred.reject(msg);
			$log.error(msg, code);
		});

		return deferred.promise;
	};

	wsvc.currentIndex = 0;


	wsvc.getNextWord = function() {
		var totalSize = wsvc.data.length;
		wsvc.currentIndex < totalSize -1 ? wsvc.currentIndex++ : wsvc.currentIndex = 0;
	};

	wsvc.getWordAsText = function(){
		if(wsvc.data.length > 0){
			return wsvc.data[wsvc.currentIndex].food	
		}
		else
		{
			return '';
		}
	};

	wsvc.getWordAsArray = function(){
		var word = wsvc.getWordAsText() || '';
		return word.split('');
	};

	wsvc.getJumbledWord = function(){
		var text = shuffle(wsvc.getWordAsArray());
		return text;
	};

	wsvc.getPointValue = function(){
		var wordCount = wsvc.getWordAsArray().length;
		var totalPoints = Math.floor(Math.pow(1.95, wordCount / 3 ))
		return totalPoints;
	};

	return wsvc;
});





// Helpers

//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/array/shuffle [v1.0]
function shuffle(o){ //v1.0
	for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
		return o;
};