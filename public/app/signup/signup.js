/**
 * Each section of the site has its own module. It probably also has
 * submodules, though this boilerplate is too simple to demonstrate it. Within
 * `src/app/home`, however, could exist several additional folders representing
 * additional modules that would then be listed as dependencies of this one.
 * For example, a `note` section could have the submodules `note.create`,
 * `note.delete`, `note.edit`, etc.
 *
 * Regardless, so long as dependencies are managed correctly, the build process
 * will automatically take take of the rest.
 *
 * The dependencies block here is also where component dependencies should be
 * specified, as shown below.
 */
 angular.module( 'ngHarpBoilerplate.signup', [
  'ngHarpBoilerplate.user',
  'ui.router'
  ])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
 .config(function config( $stateProvider ) {
  $stateProvider.state( 'signup', {
    url: '/signup',
    views: {
      "main": {
        controller: 'SignUpController',
        controllerAs: 'board',
        templateUrl: 'app/signup/signup.html',
        resolve: {
          users: ['Users', function(Users){
            return Users;
          }]
        }
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

/**
 * And of course we define a controller for our route.
 */
 .controller( 'SignUpController', ['$scope', '$state' ,'users', function($scope, $state, users) {

  $scope.loggedIn = users.loggedInCheck;
  
  $scope.usvc = users;
  
  var currentUser = Parse.User.current();
  var placeholderEmail = 'placeholdr@test.com';
  if((currentUser)){
    $scope.currentUser = currentUser;
    $scope.gravatar = users.getGravatar(currentUser.attributes.email || placeholderEmail);
  }else{
    $scope.gravatar = users.getGravatar(placeholderEmail);
  }

  $scope.getGravatar = function(){
    $scope.gravatar = users.getGravatar($scope.user.email);
  };

  $scope.submit = function(){
    users.signUp($scope.user).then(
      function(data){
        $state.transitionTo('gameboard');
      },function(err){
        console.log(err);
      });
    $scope.resetForm();
  }

  $scope.master = {};
  $scope.resetForm = function(){
    $scope.user = angular.copy($scope.master);
  }

  $scope.logOut = function(){
    users.logOut();
  };



  // Load Services



}])


