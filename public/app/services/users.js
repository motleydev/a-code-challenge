angular.module( 'ngHarpBoilerplate.user', ['angular-md5'])
.factory('Users', function(md5){
	
	usvc = {};
	
	usvc.listUsers = function(){
		var Users = Parse.Object.extend("User");
		var query = new Parse.Query(Users);
		query.select("highscore", "username");
		return query.find();
	};

	usvc.getGameDetails = function(userid){
		var Users = Parse.Object.extend("User");
		var query = new Parse.Query(Users);
		return query.get(userid);
	}

	usvc.addHighScore = function(matchObj){
		var currUser = Parse.User.current();
		var matchData = angular.copy(matchObj);
		if(!currUser.attributes.highscore)
		{
			currUser.set("highscore", matchData);	
			console.log(matchData);
			return Parse.User.current().save();	
		}
		else
		{
			if (currUser.attributes.highscore.points.value < matchData.points.value)
			{
				console.log(matchData);
				currUser.set("highscore", matchData);	
			}
			// Need to return 'empty promise' here;
			return Parse.User.current().save();
		}

	};

	usvc.signUp = function(userObj){
		var user = new Parse.User();
		user.set("name", userObj.name);
		user.set("username", userObj.username);
		user.set("password", userObj.password);
		user.set("email", userObj.email);
		user.set("gravatar", usvc.getGravatar(userObj.email));

		return user.signUp();
	};

	usvc.logIn = function(user){
		return Parse.User.logIn(user.username, user.password);
	};

	usvc.logOut = function(){
		return Parse.User.logOut();
	};
	
	usvc.getGravatar = function(email){
		var hashedEmail = md5.createHash(email);
		var gravUrl = 'http://www.gravatar.com/avatar/';
		var getUrl = window.location;
		var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
		var defaultImage = encodeURIComponent(baseUrl+'img/default-gravatar.png');
		var opts = '?d='+defaultImage+'&s=400';

		var searchString = gravUrl+hashedEmail+opts;

		return searchString;

	};
	
	usvc.loggedInCheck = function(){
		var currentUser = Parse.User.current();

		if(currentUser){
			return true;
		}
		else
		{
			return false;
		}
	}

	return usvc;

});
